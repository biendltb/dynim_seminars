# DYNIM INTERNAL SEMINARS

## AI seminars

* Slides: https://docs.google.com/presentation/d/1rumB9aUlvx7kS85QU13FRnIs2dgaw-HNaJTl8zJNaHg/edit?usp=sharing

For labs:
* labs_preview: preview labs
* labs: notebooks for all labs
    
    To run the notebook in your machine: 
    
    1) Install required packages <br/>
    `pip install -r src/demo/hrm_cam/requirements.txt`
    
    2) Navigate to the notebook forlder
    
    3) Run command: `jupyter notebook`