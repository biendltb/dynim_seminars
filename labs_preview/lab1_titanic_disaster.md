# <center> Lab 1: Basics of neural networks </center>
<center> Bien @ Dynimlabs </center> <br/>



In this lab, we will build a simple neural network with basic operations from scratch to predict who survived in the Titanic disaster. This practice will let us understand essential algorithms behind well-know AI frameworks such as Tensorflow, PyTorch, etc.

<img src='../data/_ims/titanic.jpg'/>

<center><i>(image source: Willy Stoewer/Bettmann/Getty Images)</i></center><br/>


```python
# import Python libraries for data process, math and visualisation
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
```


```python
data_path = '../data/titanic_train.csv'

raw_data_df = pd.read_csv(data_path)

raw_data_df.head()
```

| Variable |	Definition	| Key |
| --- | --- | ---|
survival |	Survival |	0 = No, 1 = Yes |
pclass	| Ticket class	| 1 = 1st (Upper), 2 = 2nd (Middle), 3 = 3rd (Lower) |
sex |	Sex	| |
fare	| Passenger fare |

## Data encoding
To keep the problem simple, only three features are selected: Sex, Ticket class and Fare. To feed the data to the network, we must encode them into numbers.

Although some features such as Sex and Ticket class could be indexed. However, the index does not represent any meaning when plotting them in the multi-dimensional space. Thus, we will use one-hot encoding method to encode them into vectors of 2 and 3 dimensions respectively. Those sparse vectors will represent the data points in the multi-dimensional space.


```python
# encode data
def encoding(df):
    """ Encode only ticket class, gender and fare
    """
    out_df = pd.get_dummies(df['Pclass'])
    out_df = pd.concat([out_df, pd.get_dummies(df['Sex']), df['Fare']], axis=1)
    return out_df

data_df = encoding(raw_data_df)
labels_df = pd.get_dummies(raw_data_df['Survived'])

data_df.head()
```

## Prepare train-eval sets

In machine learning, it is a common practice to separate your dataset into three different sets:
* Training set: the main part of dataset, used for training your model
* Evaluation set: used for evaluating your model's performance and adjust hyper-parameters
* Test set: an independent set. This one is usually used in production to make sure the model performance are maintained.

In this lab, we only use two set which is train and eval set for simplification.


```python
# split train-eval sets

np.random.seed(17)

shuffled_indexes = np.arange(len(data_df))
np.random.shuffle(shuffled_indexes)

# use 600 samples for training and the rest for evaluating
train_ids = shuffled_indexes[:600]
eval_ids = shuffled_indexes[600:]

print('Train data size: {}\neval data size: {}'.format(len(train_ids), len(eval_ids)))

train_data = data_df.iloc[train_ids].values
train_labels = labels_df.iloc[train_ids].values

eval_data = data_df.iloc[eval_ids].values
eval_labels = labels_df.iloc[eval_ids].values
```

## Network architecture

<img src='../data/_ims/titanic_net.png'/>


```python
# Network architecture: 6-dimensional input -> 1 layers: 6 nodes -> 2-dimensional output layer
def get_rand_w():
    """ Initialize weights and biases randomly"""
    # first layers
    W_1 = np.random.randn(6, 6)
    b_1 = np.random.randn(1, 6)
    # second layers
    W_2 = np.random.randn(6, 2)
    b_2 = np.random.randn(1, 2)
    
    return [W_1, b_1, W_2, b_2]

def forward_pass(x, w):
    # activation function: sigmoid
    f = lambda x: 1.0/(1.0 + np.exp(-x))
    
    W_1, b_1, W_2, b_2 = w
    
    # pass through layer 1
    out = f(np.dot(x, W_1) + b_1)
    # pass through layer 2
    out = f(np.dot(x, W_2) + b_2)
    
    return out

def get_loss(output, y):
    # calculate the loss
    loss = np.mean((y-output)**2)
    
    return loss
```


```python
%%time
epochs = 100000

# initialise the first weights and biases for the network
best_w = get_rand_w()
out = forward_pass(train_data, best_w)
best_loss = get_loss(out, train_labels)
print('Original loss: {}'.format(best_loss))

# find the optimal model by random search
for i in range(epochs):
    w = get_rand_w()
    out = forward_pass(train_data, w)
    loss = get_loss(out, train_labels)
    
    print('Epoch {}: best loss: {} | loss: {}'.format(i+1, best_loss, loss))
    
    if loss < best_loss:
        best_w = w
        best_loss = loss
```


```python
# Testing on test data
out = forward_pass(test_data, best_w)
# Get the higher value to form a binary output
pred = np.argmax(out, axis=1)
# undo the hot encode to get list of correct answers
ground_truth = np.argmax(test_labels, axis=1)

print('Test result: {:.2f}%'.format((1 - np.sum(np.abs(pred-ground_truth))/len(pred))*100))
```


```python
plt.imshow(best_w[0])
```

## Network implement in Tensorflow

If we have not been familiar with calculating the chain rule for the gradient, we could you a very simple and neat library in which the gradient are implemented.


```python
import tensorflow as tf

model = tf.keras.models.Sequential([
  tf.keras.layers.Dense(6, activation='sigmoid'),
  tf.keras.layers.Dense(2, activation='sigmoid')
])

optimizer = tf.keras.optimizers.Adam(lr=3e-4)
model.compile(optimizer='adam',
              loss='mean_squared_error',
              metrics=['binary_accuracy'])
```


```python
%%time
history = model.fit(train_data, train_labels, epochs=100)
```


```python
model.summary()
```


```python
plt.figure(figsize=(14, 6))
plt.subplot(1, 2, 1)
plt.plot(history.history['loss'])
plt.title('loss')
plt.subplot(1, 2, 2)
plt.plot(history.history['binary_accuracy'])
plt.title('accuracy')
```


```python
%%time
model.evaluate(eval_data,  eval_labels, verbose=2)
```

## Appendix 1: Calculate the gradient using chain rule expression

At here, I implement the gradient calculation with the first derivative. The mathematic concepts here will be updated.


```python
def train_step(x, y, w, lr = 1e-3):
    """ Warning: could encounter the overflow error
    """
    # FORWARD PASS
    W_1, b_1, W_2, b_2 = w
    f = lambda x: 1.0/(1.0 + np.exp(-x))
    # pass through layer 1
    out_l1 = np.dot(x, W_1) + b_1

    out_s1 = f(out_l1)

    # pass through layer 2
    out_l2 = np.dot(x, W_2) + b_2

    out_s2 = f(out_l2)

    loss = get_loss(out_s2, y)

    # BACKWARD PASS
    grad = out_s2 - y

    d_h2 = (1 - out_s2) * out_s2 * grad
    
    # Accumulate the gradient come from all examples
    d_W2 = out_s1.T.dot(d_h2)
    d_b2 = np.sum(d_h2, axis=0, keepdims=True)

    # sum of gradient come out from prev node:
    grad_1 = np.sum(d_W2.T, axis=0, keepdims=True)
    d_h1 = (1 - out_l1) * out_l1 * grad_1

    d_W1 = x.T.dot(d_h1)
    d_b1 = np.sum(d_h1, axis=0, keepdims=True)

    W_1 -= d_W1 * lr
    b_1 -= d_b1 * lr

    W_2 -= d_W2 * lr
    b_2 -= d_b2 * lr
    
    return [W_1, b_1, W_2, b_2], loss

def gd_train():
    """ Train the network with gradient decent
    """
    w = get_rand_w()

    x = train_data
    y = train_labels
    
    epochs = 100000
    lr = 1e-3

    for i in range(epochs):
        w, loss = train_step(x, y, w, lr=lr)
        if np.isnan(loss):
            break
        print('Epoch: {} Loss: {}'.format(i, loss))
```


```python
gd_train()
```


```python

```
