# <center>Classify clothing images with Fashion-MNIST dataset</center>
<img src='../data/_ims/fashion-mnist.png'><br/>


```python
import warnings
warnings.filterwarnings('ignore')

import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.datasets import fashion_mnist
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np


# -------> For RTX NVIDIA GPU only
from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

config = ConfigProto()
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)
```


```python
# Load dataset
(train_ims, train_labels), (test_ims, test_labels) = fashion_mnist.load_data()

def preprocess(ims):
    """ Normalise and add one more dimension to each image
    """
    ims = ims.astype('float32') / 255
    ims = np.expand_dims(ims, axis=-1)
    return ims

train_ims = preprocess(train_ims)
test_ims = preprocess(test_ims)
```


```python
# visualise the data
print('Train size: {} | Test size: {}'.format(len(train_ims), len(test_ims)))
plt.figure(figsize=(8, 8))
for i in range(49):
    plt.subplot(7, 7, i+1)
    plt.imshow(train_ims[i][:, :, 0], cmap='gray')
plt.show()

print('Dataset shape: {}'.format(train_ims.shape))
```

## Exercise: create your own network with tensorflow

Basic layers:
* Convolutional layer: https://www.tensorflow.org/api_docs/python/tf/keras/layers/Conv2D
* Max-pooling layer: https://www.tensorflow.org/api_docs/python/tf/keras/layers/MaxPool2D
* Flatten: https://www.tensorflow.org/api_docs/python/tf/keras/layers/Flatten
* Dense: https://www.tensorflow.org/api_docs/python/tf/keras/layers/Dense

Hyper-parameter to be changed:
* learning rate
* optimizer
* layer size or filters
* kernel size
* activation
* model depth


```python
def create_example_model():
    model = tf.keras.models.Sequential()
    model.add(layers.Conv2D(16, (5, 5), activation='relu', input_shape=(28, 28, 1)))
    model.add(layers.MaxPooling2D((2, 2)))

    model.add(layers.Conv2D(32, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D((2, 2)))

    # example output part of the model
    model.add(layers.Flatten())
    model.add(layers.Dense(128, activation='relu'))
    model.add(layers.Dense(10, activation='softmax'))
    
    return model

def my_model():
    model = tf.keras.models.Sequential()
    
    # YOUR CODE HERE
    
    return model
```


```python
# Call your model here
model = create_example_model()

opt = tf.keras.optimizers.SGD(lr=0.05, momentum=0.9)
model.compile(optimizer=opt, loss='sparse_categorical_crossentropy', metrics=['accuracy'])

model.summary()
```


```python
%%time
history = model.fit(train_ims, train_labels, epochs=20, batch_size=128)

# Visualising the process
plt.figure(figsize=(14, 6))
plt.subplot(1, 2, 1)
plt.plot(history.history['loss'])
plt.title('loss')
plt.subplot(1, 2, 2)
plt.plot(history.history['accuracy'])
plt.title('accuracy')
plt.show()
```


```python
results = model.evaluate(test_ims,  test_labels, verbose=0)
print('Evaluation loss: {:.4f} | Accuracy: {:.2f} %'.format(results[0], results[1]*100))
```

## Testing the model from a hand-drawing images


```python
def im_predict(model, gray_im):
    classes_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat', 'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle', 'boot']
    plt.imshow(gray_im, cmap='gray')
    gray_im = np.expand_dims(gray_im, axis=-1)
    
    out = model.predict([[gray_im]])
    
    sorted_result = np.argsort(out[0])
    for i in sorted_result[::-1]:
        print('Class: {} | Probability: {:.2f}%'.format(classes_names[i], out[0, i] * 100))


test_im = mpimg.imread('../data/hand_drawings/003.png')
im_predict(model, test_im)

```

## Testing with image from a URL


```python
from PIL import Image
import requests
from io import BytesIO

url = 'https://de9luwq5d40h2.cloudfront.net/catalog/product/large_image/09_407044.jpg'
response = requests.get(url)
im = Image.open(BytesIO(response.content))
plt.imshow(im)
```


```python
# convert to grayscale
im = im.convert('L')
# resize image and convert to float
im = np.array(im.resize((28, 28)))/225.0

# Note: make the background dark and object bright to be same with the training set
im = 1 - im

im_predict(model, im)
```


```python

```
